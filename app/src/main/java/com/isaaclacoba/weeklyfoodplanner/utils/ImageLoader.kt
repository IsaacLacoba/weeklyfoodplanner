package com.isaaclacoba.weeklyfoodplanner.utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class ImageLoader(
        private val context: Context
) {
    fun loadImage(
            imageUrl: String?,
            view: ImageView) {
        val options = RequestOptions().centerCrop()
        Glide.with(context)
            .asBitmap()
            .apply(options)
            .load(imageUrl).into(view)
    }
}