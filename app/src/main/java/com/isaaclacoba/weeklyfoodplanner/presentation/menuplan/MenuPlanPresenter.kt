package com.isaaclacoba.weeklyfoodplanner.presentation.menuplan

import com.isaaclacoba.weeklyfoodplanner.base.BasePresenter
import com.isaaclacoba.weeklyfoodplanner.domain.logout.LogoutUseCase
import com.isaaclacoba.weeklyfoodplanner.domain.menu.SearchForMenuUseCase
import com.isaaclacoba.weeklyfoodplanner.domain.reactive.addDisposableTo
import com.isaaclacoba.weeklyfoodplanner.utils.formatDate
import com.isaaclacoba.weeklyfoodplanner.utils.getTodayDate
import io.reactivex.rxkotlin.subscribeBy

class MenuPlanPresenter(
    private val searchForMenuUseCase: SearchForMenuUseCase,
    private val logoutUseCase: LogoutUseCase
): BasePresenter<MenuPlanListView>() {

    override fun onCreate() {
        super.onCreate()
        getMenuDay(getTodayDate())
    }

    private fun getMenuDay(date: String) {
        searchForMenuUseCase.execute(date).subscribeBy(
            onNext = {
                getView()?.showDayMenu(it.toUI())
            },
            onError = {
                getView()?.showMenuError()
            }
        ).addDisposableTo(disposeBag)
    }

    fun onMenuPressed(menu: MenuItemUI) {
    }

    fun onDateChange(year: Int, month: Int, dayOfMonth: Int) {
        getMenuDay(formatDate(year, month, dayOfMonth))
    }

    fun logout() {
        logoutUseCase.execute("logout-random-string").subscribeBy(
            onNext = {
                getView()?.logout()
            },
            onError = {

            }
        ).addDisposableTo(disposeBag)
    }
}