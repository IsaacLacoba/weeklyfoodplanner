package com.isaaclacoba.weeklyfoodplanner.presentation.menuplan


import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.isaaclacoba.weeklyfoodplanner.R
import com.isaaclacoba.weeklyfoodplanner.WFPApplication
import com.isaaclacoba.weeklyfoodplanner.base.BaseActivity
import com.isaaclacoba.weeklyfoodplanner.lifecycle.Presenter
import com.isaaclacoba.weeklyfoodplanner.presentation.login.LoginActivity
import com.isaaclacoba.weeklyfoodplanner.presentation.login.LoginActivity_MembersInjector
import com.isaaclacoba.weeklyfoodplanner.presentation.menuplan.adapter.MenuItemRenderer
import com.isaaclacoba.weeklyfoodplanner.presentation.menuplan.injector.DaggerMenuPlanComponent
import com.isaaclacoba.weeklyfoodplanner.presentation.menuplan.injector.MenuPlanModule
import com.isaaclacoba.weeklyfoodplanner.presentation.recipe.RecipeActivity
import com.pedrogomez.renderers.ListAdapteeCollection
import com.pedrogomez.renderers.RVRendererAdapter
import com.pedrogomez.renderers.RendererBuilder
import kotlinx.android.synthetic.main.menuplan_layout.*
import javax.inject.Inject

class MenuPlanActivity : BaseActivity(), MenuPlanListView{
    override val layoutRes: Int = R.layout.menuplan_layout
    override val hasSupportActionBar = true
    override val hasDisplayHomeAsUpEnabled = false

    private lateinit var dayMenuAdapter: RVRendererAdapter<MenuUI>

    @Presenter
    @Inject
    lateinit var presenter: MenuPlanPresenter

    @Inject
    lateinit var googleSignIn: GoogleSignInClient

    override fun initInjector() {
        DaggerMenuPlanComponent.builder()
            .applicationComponent(WFPApplication.applicationComponent)
            .menuPlanModule(MenuPlanModule(this))
            .build()
            .inject(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.logout_action -> {
                presenter.logout()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setupViews() {
        super.setupViews()
        setupCustomDatePicker()
        setupAdapter()
        setupRecyclerView()
        setupFloatingButton()
    }

    private fun setupCustomDatePicker() {
        custom_date_picker.setOnDateChangeListener( presenter )
        custom_date_picker.setOnClickListeners(presenter)
    }

    private fun setupAdapter() {
        val rendererBuilder = RendererBuilder<MenuUI>()
            .bind(MenuItemUI::class.java, MenuItemRenderer { presenter.onMenuPressed(it) })
        dayMenuAdapter = RVRendererAdapter(rendererBuilder, ListAdapteeCollection())
    }

    private fun setupRecyclerView() {
        with(dayMenuList) {
            layoutManager = LinearLayoutManager(this@MenuPlanActivity, LinearLayout.VERTICAL, false)
            setHasFixedSize(true)
            adapter = dayMenuAdapter
            addOnScrollListener(menuListOnScrollListener)
        }
    }


    private fun setupFloatingButton() {
        add_recipe.setOnClickListener {
            startActivity(Intent(this, RecipeActivity::class.java))
        }
    }

    private val menuListOnScrollListener = object : RecyclerView.OnScrollListener() {
    }

    override fun showDayMenu(menuList: List<MenuUI>) {
        dayMenuAdapter.diffUpdate(menuList)
    }

    override fun showMenuError() {
        TODO("not implemented")
    }

    override fun logout() {
        googleSignIn.signOut()
        backToLoginView()
    }

    override fun backToLoginView() {
        startActivity(Intent(applicationContext, LoginActivity::class.java))
    }
}