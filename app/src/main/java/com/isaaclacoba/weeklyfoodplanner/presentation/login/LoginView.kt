package com.isaaclacoba.weeklyfoodplanner.presentation.login

import com.isaaclacoba.weeklyfoodplanner.base.BaseView

interface LoginView: BaseView {
    fun loginCompleted()
    fun showLoginError(msg: String)
}