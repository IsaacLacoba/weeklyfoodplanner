package com.isaaclacoba.weeklyfoodplanner.data.client

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.database.*
import com.isaaclacoba.weeklyfoodplanner.domain.menu.MenuItem
import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeItem
import com.isaaclacoba.weeklyfoodplanner.data.menu.SearchMenuApiModel
import com.isaaclacoba.weeklyfoodplanner.data.recipe.RecipeApiItem
import com.isaaclacoba.weeklyfoodplanner.data.recipe.toApiData
import com.isaaclacoba.weeklyfoodplanner.domain.logger.Logger
import com.isaaclacoba.weeklyfoodplanner.domain.logout.ApiLogoutItem
import com.isaaclacoba.weeklyfoodplanner.domain.logout.LogoutItem
import io.reactivex.Completable
import io.reactivex.Observable

class FirebaseApiClient (
    val firebaseAuth: FirebaseAuth,
    val firebaseDatabase: FirebaseDatabase
) {
    init {

    }

    fun signInWithGoogle(idToken: String): Observable<String> {
        Logger.d { "FirebaseClient signInWithGoogle" }
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        return Observable.create { observer ->
            firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        val user = firebaseAuth.currentUser
                        observer.onNext(user.toString())
                    } else {
                        // If sign in fails, display a message to the user.
                        observer.onError(it.exception!!)
                    }
                }
        }
    }

    fun logout(username: String): Observable<ApiLogoutItem> {
        return Observable.create { observer ->
            firebaseAuth.signOut()
            observer.onNext(ApiLogoutItem(LogoutItem(username), result = "Success"))
        }
    }

    fun getMenu(date: String): Observable<SearchMenuApiModel> {

        val recipe = RecipeItem(
            name = "Great Recipe",
            moment = "Breakfast",
            imageUrl = "https://cdn.pixabay.com/photo/2017/01/31/09/30/raspberry-2023404_1280.jpg",
            description = "yummy recipe",
            ingredients = listOf()
        )

        var recipe2 = recipe.copy(moment = "Lunch",
            imageUrl = "https://cdn.pixabay.com/photo/2017/12/09/08/18/pizza-3007395_1280.jpg")

        var recipe3 = recipe.copy(moment = "Dinner",
            imageUrl = "https://cdn.pixabay.com/photo/2016/11/23/13/45/blur-1852926_1280.jpg")

        val menuItem = MenuItem(
            name = "sample 1",
            date = "2018/12/09",
            imageUrl = "https://cdn.pixabay.com/photo/2016/08/25/07/02/yummy-1618876_960_720.jpg",
            description = "mmm so Yummy!!",
            recipeList = listOf(recipe, recipe2, recipe3))


        //Todo: Use Firebase API instead of this Mock
        return Observable.just(SearchMenuApiModel(date, menuItem))
    }

    fun getRecipes(): Observable<List<RecipeApiItem>> {
        val refRecipe = firebaseDatabase.reference.child("recipes")
        return Observable.create<List<RecipeApiItem>> { emitter ->
            val listener = object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    Logger.d { "getRecipes cancelled" }
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    Logger.d { "getRecipes onDataChange. Creating list of recipes" }
                    emitter.onNext(snapshot.children.map { it.getValue(RecipeApiItem::class.java) } as List<RecipeApiItem>)
                }
            }

            refRecipe.addValueEventListener(listener)
        }
    }

    fun addRecipe(recipe: RecipeItem):Observable<RecipeItem> {
        Logger.d { "FirebaseClient addRecipe $recipe" }
        val recipesReference = firebaseDatabase.reference.child("recipes")
        recipesReference.child(recipe.name).setValue(recipe.toApiData())
            .addOnSuccessListener {
                Logger.d { "insert completed" }
            }
            .addOnFailureListener {
                Logger.e { "Baia Baia, It fails. Error message: ${it.message}" }
            }
        return Observable.just(recipe)
    }

    fun deleteRecipe(key:String): Completable {
        Logger.d { "FirebaseClient deleteRecipe $key" }
        val recipeRef = firebaseDatabase.reference.child("recipes").child(key).removeValue()
        return Completable.complete()
    }
}