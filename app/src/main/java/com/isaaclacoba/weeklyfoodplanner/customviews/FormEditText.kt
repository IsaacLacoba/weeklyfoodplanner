package com.isaaclacoba.weeklyfoodplanner.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.isaaclacoba.weeklyfoodplanner.R
import com.isaaclacoba.weeklyfoodplanner.domain.logger.Logger
import com.isaaclacoba.weeklyfoodplanner.utils.loadFormTextView


class FormEditText(context: Context?, attrs: AttributeSet?): LinearLayout(context, attrs) {
    private val editText: EditText by lazy { rootView.findViewById<EditText>(R.id.edit_text) }
    private val textView: TextView by lazy { rootView.findViewById<TextView>(R.id.title) }

    var text: String = ""
        get() = editText.text.toString()

    init {
        LayoutInflater.from(context).inflate(R.layout.form_edit_text, this, true)

        val attributes = context!!.obtainStyledAttributes(attrs, R.styleable.FormEditText)
        editText.hint = attributes.getString(R.styleable.FormEditText_hint)

        loadFormTextView(context, attrs, textView)
    }
}