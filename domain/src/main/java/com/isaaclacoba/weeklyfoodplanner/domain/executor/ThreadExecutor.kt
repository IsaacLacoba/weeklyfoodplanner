package com.isaaclacoba.weeklyfoodplanner.domain.executor

import java.util.concurrent.Executor

interface ThreadExecutor : Executor