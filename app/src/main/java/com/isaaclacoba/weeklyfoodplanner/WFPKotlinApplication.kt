package com.isaaclacoba.weeklyfoodplanner

import android.app.Application
import com.isaaclacoba.weeklyfoodplanner.domain.logger.Kog
import com.isaaclacoba.weeklyfoodplanner.domain.logger.Logger
import com.isaaclacoba.weeklyfoodplanner.injection.component.ApplicationComponent
import com.isaaclacoba.weeklyfoodplanner.injection.component.DaggerApplicationComponent
import com.isaaclacoba.weeklyfoodplanner.injection.modules.ApplicationModule
import com.isaaclacoba.weeklyfoodplanner.logger.AndroidLogger

class WFPApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        WFPApplication.application = this
        initLogger()
    }

    private fun initLogger() {
        Kog.plant(AndroidLogger())
        Logger.i {"Logger planted"}
    }

    companion object {
        private lateinit var application: WFPApplication
        val applicationComponent: ApplicationComponent by lazy {
            val appComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(application))
                .build()
            appComponent.inject(application)
            appComponent
        }
    }
}