package com.isaaclacoba.weeklyfoodplanner.presentation.recipe.utils

import android.content.Context
import android.support.v7.app.AlertDialog
import android.widget.Button
import com.isaaclacoba.weeklyfoodplanner.R
import com.isaaclacoba.weeklyfoodplanner.customviews.FormEditText
import com.isaaclacoba.weeklyfoodplanner.customviews.FormSpinner
import com.isaaclacoba.weeklyfoodplanner.domain.menu.IngredientItem
import com.isaaclacoba.weeklyfoodplanner.presentation.recipe.RecipeItemUI

fun createRecipeDialog(
    context: Context,
    acceptListener: (RecipeItemUI) -> Unit,
    canceListener: () -> Unit): AlertDialog {

    val cancelButton: Button?
    val okButton: Button?
    val eatTimeSpinner: FormSpinner
    val recipeNameEditText: FormEditText

    val alertDialog: AlertDialog = AlertDialog.Builder(context)
        .setView(R.layout.add_recipe_dialog_layout)
        .create()

    alertDialog.show()

    eatTimeSpinner = alertDialog.findViewById(R.id.moment_of_day_spinner)!!
    recipeNameEditText = alertDialog.findViewById(R.id.recipe_name_fet)!!

    cancelButton = alertDialog.findViewById(R.id.cancel_btn)
    cancelButton!!.setOnClickListener {
        canceListener()
        alertDialog.dismiss()
    }


    okButton = alertDialog.findViewById(R.id.ok_btn)
    okButton!!.setOnClickListener {
        if (!recipeNameEditText.text.isEmpty()) {
            //ToDo: add image URL, description and ingredients list
            val recipe = RecipeItemUI(name = recipeNameEditText.text,
                moment = eatTimeSpinner.selectedItem,
                imageUrl = getImageUrl(context = context,
                    moment = eatTimeSpinner.selectedItem),
                description = "",
                ingredients = listOf(
                    IngredientItem(
                        name = recipeNameEditText.text,
                        imageUrl = "",
                        description = eatTimeSpinner!!.selectedItem)
                )
            )
            acceptListener(recipe)
        }

        alertDialog.dismiss()
    }

    return alertDialog
}

private fun getImageUrl(context: Context, moment: String): String =
    when (moment) {
        "Breakfast" -> context.getString(R.string.breakfast_image_url)
        "Lunch" -> context.getString(R.string.lunch_image_url)
        "Dinner" -> context.getString(R.string.dinner_image_url)
        else -> ""
    }