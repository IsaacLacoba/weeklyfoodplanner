package com.isaaclacoba.weeklyfoodplanner.presentation.recipe.adapter

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import com.isaaclacoba.weeklyfoodplanner.presentation.recipe.RecipeItemUI
import com.isaaclacoba.weeklyfoodplanner.presentation.recipe.RecipeUI
import com.isaaclacoba.weeklyfoodplanner.presentation.recipe.utils.addOrUpdate
import com.pedrogomez.renderers.RVRendererAdapter

interface IRecipeManager {
    fun loadRecipeList(recipeList: List<RecipeUI>)
    fun addRecipe(recipe: RecipeUI)
    fun removeRecipe(recipePosition: Int): RecipeUI
    fun undoDelete(view: View): RecipeUI

    fun contains(recipe: RecipeUI): Boolean
}

class RecipeManager(
    private val recipeListAdapter: RVRendererAdapter<RecipeUI>,
    private val linearLayoutManager: LinearLayoutManager,
    private val recyclerView: RecyclerView,
    private val recypeListOnScrollListener:  RecyclerView.OnScrollListener,
    private val rvTouchHelper: ItemTouchHelper,
    var recipeList: MutableList<RecipeUI> = mutableListOf()): IRecipeManager {

    var lastItemRemoved: Int  = 0
    var lastRecipeRemoved: RecipeUI? = null

    init {
        with(recyclerView) {
            layoutManager = linearLayoutManager
            setHasFixedSize(true)
            adapter = recipeListAdapter
            addOnScrollListener(recypeListOnScrollListener)
            rvTouchHelper.attachToRecyclerView(this)
        }
    }

    override fun loadRecipeList(recipeList: List<RecipeUI>) {
        this.recipeList = recipeList as MutableList<RecipeUI>
        recipeListAdapter.diffUpdate(recipeList)
        recipeListAdapter.notifyDataSetChanged()
    }

    override fun addRecipe(recipe: RecipeUI) {
        recipeListAdapter.addOrUpdate(recipe)
        recipeListAdapter.notifyDataSetChanged()
        recipeList.add(recipe)
    }

    override fun removeRecipe(recipePosition: Int): RecipeUI {
        val recipeUI = recipeListAdapter.getItem(recipePosition) as RecipeUI
        recipeListAdapter.remove(recipeUI)
        recipeListAdapter.notifyItemRemoved(recipePosition)

        lastItemRemoved = recipePosition
        lastRecipeRemoved = recipeUI as RecipeItemUI?
        recipeList.removeAt(recipePosition)
        return lastRecipeRemoved!!
    }

    override fun undoDelete(view: View): RecipeUI {
        recipeList.add(lastItemRemoved, lastRecipeRemoved!!)
        loadRecipeList(recipeList)
        return lastRecipeRemoved as RecipeUI
    }

    override fun contains(recipe: RecipeUI) = recipeList.contains(recipe)

    data class Builder(
        var recipeListAdapter: RVRendererAdapter<RecipeUI>? = null,
        var linearLayoutManager: LinearLayoutManager? = null,
        var recyclerView: RecyclerView? = null,
        var recipeListOnScrollListener:  RecyclerView.OnScrollListener? = null,
        var rvTouchHelper: ItemTouchHelper? = null) {

        fun setRecipeListAdapter(recipeListAdapter: RVRendererAdapter<RecipeUI>) = apply {
            this.recipeListAdapter = recipeListAdapter
        }

        fun setLinearLayout(linearLayout: LinearLayoutManager) = apply {
            this.linearLayoutManager = linearLayout
        }

        fun setRecyclerView(recyclerView: RecyclerView) = apply {
            this.recyclerView = recyclerView
        }

        fun setRecyclerListOnScrollListener(recypeListOnScrollListener: RecyclerView.OnScrollListener) =
            apply {
                this.recipeListOnScrollListener = recypeListOnScrollListener
            }

        fun setRvTouchHelper(rvTouchHelper: ItemTouchHelper) = apply {
            this.rvTouchHelper = rvTouchHelper
        }

        fun build() = RecipeManager(recipeListAdapter!!,
            linearLayoutManager!!, recyclerView!!, recipeListOnScrollListener!!, rvTouchHelper!!)
    }
}