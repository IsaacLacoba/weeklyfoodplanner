package com.isaaclacoba.weeklyfoodplanner.presentation.recipe

import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.widget.LinearLayout
import com.isaaclacoba.weeklyfoodplanner.base.BaseActivity
import com.isaaclacoba.weeklyfoodplanner.WFPApplication
import com.isaaclacoba.weeklyfoodplanner.domain.logger.Logger
import com.isaaclacoba.weeklyfoodplanner.lifecycle.Presenter
import com.isaaclacoba.weeklyfoodplanner.presentation.recipe.adapter.RecipeItemRenderer
import com.isaaclacoba.weeklyfoodplanner.presentation.recipe.injector.DaggerRecipeComponent
import com.isaaclacoba.weeklyfoodplanner.presentation.recipe.injector.RecipeModule
import com.isaaclacoba.weeklyfoodplanner.presentation.recipe.utils.createRecipeDialog
import com.isaaclacoba.weeklyfoodplanner.utils.RVItemTouchHelper
import com.pedrogomez.renderers.ListAdapteeCollection
import com.pedrogomez.renderers.RVRendererAdapter
import com.pedrogomez.renderers.RendererBuilder
import javax.inject.Inject
import android.support.design.widget.Snackbar
import android.view.View
import com.isaaclacoba.weeklyfoodplanner.R
import com.isaaclacoba.weeklyfoodplanner.presentation.recipe.adapter.RecipeManager
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class RecipeActivity: BaseActivity(), RecipeView {
    private val LOG_TAG = "RecipeActivity"

    @Presenter
    @Inject
    lateinit var presenter: RecipePresenter

    override val layoutRes : Int = R.layout.recipe_activity
    override val hasSupportActionBar = true
    override val hasDisplayHomeAsUpEnabled = true

    private lateinit var recipeManager: RecipeManager

    private val floatingButton: FloatingActionButton by lazy { findViewById<FloatingActionButton>(R.id.add_recipe) }

    override fun initInjector() {
        Logger.d {"$LOG_TAG initInjector"}
        DaggerRecipeComponent.builder()
            .applicationComponent(WFPApplication.applicationComponent)
            .recipeModule(RecipeModule(this))
            .build()
            .inject(this)
    }

    override fun setupViews() {
        super.setupViews()
        supportActionBar?.title = getString(R.string.recipe_title)
        setupRecipeManager()

        floatingButton.setOnClickListener {
            createRecipeDialog(context = RecipeActivity@this,
                acceptListener =  {
                    Logger.d { "adding new recipe $it" }
                    addRecipe(recipe = it)
                },
                canceListener =  {
                    Logger.d { "addRecipeDialog cancelled" }
                })
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.getRecipes()
    }

    //RecipeManager encapsulate all the logic related with the recipe actions(add, remove,...) along with
    private fun setupRecipeManager() {
        val rendererBuilder = RendererBuilder<RecipeUI>()
            .bind(RecipeUI::class.java,
                RecipeItemRenderer { Logger.d { "$LOG_TAG recipe row clicked" } }) //ViewHolder onClick listener

        recipeManager = RecipeManager.Builder()
            .setLinearLayout(LinearLayoutManager(this@RecipeActivity, LinearLayout.VERTICAL, false))
            .setRecipeListAdapter(RVRendererAdapter(rendererBuilder, ListAdapteeCollection()))
            .setRvTouchHelper(ItemTouchHelper(RVItemTouchHelper(::removeRecipe)))
            .setRecyclerView(findViewById(R.id.recipe_rv) )
            .setRecyclerListOnScrollListener(object: RecyclerView.OnScrollListener() {})
            .build()
    }

    override fun showRecipeList(menuList: List<RecipeUI>) {
        recipeManager.loadRecipeList(menuList)
    }

    override fun addRecipe(recipe: RecipeItemUI) {
        recipeManager.addRecipe(recipe)
        presenter.addRecipe(recipe)
    }

    override fun removeRecipe(recipePosition: Int) {
        val recipeUI = recipeManager.removeRecipe(recipePosition)
        showUndoSnackbar()
        GlobalScope.launch {
            delay(3500) //wait the same time as the snackbar is going to be visible
            if (!recipeManager.contains(recipeUI)) {
                presenter.removeRecipe(recipeUI)
            }
        }
    }

    private fun showUndoSnackbar() {
        val view = findViewById<CoordinatorLayout>(R.id.recipe_activity_coordinator_layout)
        val snackbar = Snackbar.make(
            view, com.isaaclacoba.weeklyfoodplanner.R.string.undo_delete_recipe_text,
            Snackbar.LENGTH_LONG
        )
        snackbar.setAction(R.string.undo_text) { undoDelete(view)}
        snackbar.show()
    }

    private fun undoDelete(view: View) {
        val lastRecipeRemoved = recipeManager.undoDelete(view)
        presenter.addRecipe(lastRecipeRemoved)
    }
    //End RecyclerView objects
}