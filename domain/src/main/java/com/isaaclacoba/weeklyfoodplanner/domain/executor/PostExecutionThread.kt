package com.isaaclacoba.weeklyfoodplanner.domain.executor

import io.reactivex.Scheduler

interface PostExecutionThread {
    fun getScheduler(): Scheduler
}
