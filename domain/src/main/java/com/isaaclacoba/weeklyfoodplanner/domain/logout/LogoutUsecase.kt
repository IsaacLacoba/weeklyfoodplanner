package com.isaaclacoba.weeklyfoodplanner.domain.logout

import com.isaaclacoba.weeklyfoodplanner.domain.executor.ThreadScheduler
import com.isaaclacoba.weeklyfoodplanner.domain.executor.applyScheduler
import io.reactivex.Observable

interface ILogoutUseCase {
    fun execute(username: String): Observable<String>
}

class LogoutUseCase(
    private val logoutResource: LogoutResource,
    private val threadScheduler: ThreadScheduler
): ILogoutUseCase {
    override fun execute(username: String): Observable<String> {
        return logoutResource.logout(username).applyScheduler(threadScheduler)
    }
}