package com.isaaclacoba.weeklyfoodplanner.domain.reactive

import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeItem
import io.reactivex.Completable
import io.reactivex.Observable

interface RecipeApi {
    fun addRecipe(recipe: RecipeItem): Observable<RecipeItem>
    fun deleteRecipe(key: String): Completable
    fun getRecipes(): Observable<List<RecipeItem>>
}