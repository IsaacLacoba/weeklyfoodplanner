package com.isaaclacoba.weeklyfoodplanner.domain.logout

import io.reactivex.Observable

interface LogoutApi {
    fun logout(username: String): Observable<ApiLogoutItem>
}