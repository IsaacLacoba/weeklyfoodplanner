package com.isaaclacoba.weeklyfoodplanner.injection.modules

import com.isaaclacoba.weeklyfoodplanner.data.login.LoginApiDataSource
import com.isaaclacoba.weeklyfoodplanner.data.repository.CachePolicyTtl
import com.isaaclacoba.weeklyfoodplanner.data.repository.RxInMemoryCacheDataSource
import com.isaaclacoba.weeklyfoodplanner.domain.login.ApiLoginItem
import com.isaaclacoba.weeklyfoodplanner.domain.login.LoginApi
import com.isaaclacoba.weeklyfoodplanner.domain.login.LoginResource
import com.isaaclacoba.weeklyfoodplanner.domain.repository.RxBaseRepository
import com.isaaclacoba.weeklyfoodplanner.domain.repository.TimeProvider

import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module( includes = [TimeProviderModule::class, FirebaseModule::class])
open class GlobalLoginModule {
    companion object {
        private val REPO_VERSION: Int = 1
    }

    @Provides
    fun providesLoginResource(
        loginRepository: RxBaseRepository<String, ApiLoginItem>
    ): LoginResource = LoginResource(loginRepository)

    @Provides
    fun providesLoginRepository(
        loginInMemoryDataSource: RxInMemoryCacheDataSource<String, ApiLoginItem>,
        loginApiDataSource: LoginApiDataSource
    ): RxBaseRepository<String, ApiLoginItem> =
            RxBaseRepository(
                rxCacheableDataSources = listOf(loginInMemoryDataSource),
                rxWriteableDataSources = listOf(),
                rxReadableDataSources = listOf(loginApiDataSource)
            )

    @Provides
    @Reusable
    fun providesLoginApiDataSource(
        loginApi: LoginApi
    ): LoginApiDataSource =
        LoginApiDataSource(loginApi)

    @Provides
    @Reusable
    fun providesLoginInMemoryDataSource(
        timeProvider: TimeProvider
    ): RxInMemoryCacheDataSource<String, ApiLoginItem> =
        RxInMemoryCacheDataSource(
            version = REPO_VERSION,
            timeProvider = timeProvider,
            policies = listOf(
                CachePolicyTtl.oneMinute(timeProvider = timeProvider)
            )
        )
}