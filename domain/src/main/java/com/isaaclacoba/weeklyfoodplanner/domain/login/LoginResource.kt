package com.isaaclacoba.weeklyfoodplanner.domain.login

import com.isaaclacoba.weeklyfoodplanner.domain.repository.RxBaseRepository
import io.reactivex.Observable

class LoginResource (
    private val loginRepository: RxBaseRepository<String, ApiLoginItem>
) {

    fun login(idToken: String): Observable<String> =
            loginRepository.getByKey(idToken).map { it.item.username }
}