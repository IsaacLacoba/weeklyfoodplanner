package com.isaaclacoba.weeklyfoodplanner.presentation.recipe.adapter

import android.support.constraint.ConstraintLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.isaaclacoba.weeklyfoodplanner.R
import com.isaaclacoba.weeklyfoodplanner.presentation.recipe.RecipeUI
import com.isaaclacoba.weeklyfoodplanner.utils.loadImage
import com.pedrogomez.renderers.Renderer
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class RecipeItemRenderer(
    private val onClick: () -> Unit = {}
): Renderer<RecipeUI>() {
    private lateinit var recipeImage: ImageView
    lateinit var recipeName: TextView
    lateinit var eatTime: TextView
    lateinit var foregroundView: ConstraintLayout
    lateinit var backgroundView: ConstraintLayout

    override fun inflate(inflater: LayoutInflater?, parent: ViewGroup?): View =
        inflater!!.inflate(R.layout.recipelist_item, parent!!, false)

    override fun setUpView(rootView: View?) {
        recipeName = rootView!!.findViewById<TextView>(R.id.name_tv)
        eatTime = rootView.findViewById<TextView>(R.id.time_to_eat_tv)
        recipeImage = rootView.findViewById<ImageView>(R.id.recipe_iv)
        backgroundView = rootView.findViewById<ConstraintLayout>(R.id.background_view)
        foregroundView = rootView.findViewById<ConstraintLayout>(R.id.foreground_view)
    }

    override fun setContent(content: RecipeUI?) {
        super.setContent(content)
        recipeName.text = content!!.name
        eatTime.text = content.moment
        recipeImage.loadImage(content.imageUrl)
    }

    override fun render() {
        val animation = AnimationUtils.loadAnimation(context, R.anim.push_left_in)
        GlobalScope.launch {
            delay(100)
            rootView.startAnimation(animation)
        }

    }

    override fun hookListeners(rootView: View?) {
        rootView?.setOnClickListener { onClick() }
    }
}