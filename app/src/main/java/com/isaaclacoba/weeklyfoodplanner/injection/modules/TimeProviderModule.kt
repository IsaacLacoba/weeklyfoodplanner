package com.isaaclacoba.weeklyfoodplanner.injection.modules

import com.isaaclacoba.weeklyfoodplanner.domain.repository.ApplicationTimeProvider
import com.isaaclacoba.weeklyfoodplanner.domain.repository.TimeProvider
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
open class TimeProviderModule {

    @Provides
    @Reusable
    open fun providesTimeProvider(): TimeProvider = ApplicationTimeProvider()
}