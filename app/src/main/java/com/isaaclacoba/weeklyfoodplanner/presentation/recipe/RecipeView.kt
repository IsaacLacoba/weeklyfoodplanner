package com.isaaclacoba.weeklyfoodplanner.presentation.recipe

import com.isaaclacoba.weeklyfoodplanner.base.BaseView
import com.isaaclacoba.weeklyfoodplanner.domain.menu.IngredientItem
import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeItem

interface RecipeView: BaseView {
    fun addRecipe(recipe: RecipeItemUI)
    fun removeRecipe(recipeIndex: Int)
    fun showRecipeList(recipeList: List<RecipeUI>)
}

interface RecipeUI {
    val name: String
    var moment: String
    var imageUrl: String
    var description: String
    var ingredients: List<IngredientItem>?

    fun update(itemToCopy: RecipeUI)
}

data class RecipeItemUI(
    override val name: String,
    override var moment: String,
    override var imageUrl: String,
    override var description: String,
    override var ingredients: List<IngredientItem>?
) : RecipeUI {
    override fun update(itemToCopy: RecipeUI) {
        moment = itemToCopy.moment
        imageUrl = itemToCopy.imageUrl
        description = itemToCopy.description
        ingredients = itemToCopy.ingredients
    }
}

fun RecipeItemUI.toDomain(): RecipeItem =
        RecipeItem (
            name = name,
            moment = moment,
            imageUrl = imageUrl,
            description = moment,
            ingredients = ingredients
        )

fun RecipeItem.toUI(): RecipeItemUI =
        RecipeItemUI(
            name = name,
            moment = moment,
            imageUrl = imageUrl,
            description = moment,
            ingredients = ingredients
        )