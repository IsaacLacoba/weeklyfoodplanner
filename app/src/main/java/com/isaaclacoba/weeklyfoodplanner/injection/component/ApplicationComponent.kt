package com.isaaclacoba.weeklyfoodplanner.injection.component

import com.isaaclacoba.weeklyfoodplanner.WFPApplication
import com.isaaclacoba.weeklyfoodplanner.domain.executor.PostExecutionThread
import com.isaaclacoba.weeklyfoodplanner.domain.executor.ThreadExecutor
import com.isaaclacoba.weeklyfoodplanner.domain.executor.ThreadScheduler
import com.isaaclacoba.weeklyfoodplanner.domain.login.LoginResource
import com.isaaclacoba.weeklyfoodplanner.domain.logout.LogoutResource
import com.isaaclacoba.weeklyfoodplanner.domain.menu.MenuResource
import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeResource
import com.isaaclacoba.weeklyfoodplanner.injection.modules.ApplicationModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent{
    fun inject(application: WFPApplication)

    fun threadScheduler(): ThreadScheduler

    fun threadExecutor(): ThreadExecutor

    fun postExecutionThread(): PostExecutionThread

    fun menuResource(): MenuResource

    fun loginResource(): LoginResource

    fun logoutResource(): LogoutResource

    fun recipeResource(): RecipeResource
}