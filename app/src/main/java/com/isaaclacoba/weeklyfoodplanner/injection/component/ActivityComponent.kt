package com.isaaclacoba.weeklyfoodplanner.injection.component

import com.isaaclacoba.weeklyfoodplanner.injection.PerActivity
import com.isaaclacoba.weeklyfoodplanner.injection.modules.ApplicationModule
import dagger.Component

@PerActivity
@Component(
        dependencies = arrayOf(ApplicationComponent::class),
        modules = arrayOf(ApplicationModule::class)
)
interface ActivityComponent {
}