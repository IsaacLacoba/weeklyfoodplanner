package com.isaaclacoba.weeklyfoodplanner.logger

//Author: https://github.com/xavisson/MarvelKotlin/commit/0f13afb1e246f42c82bbd462ee1cd079f284bc6f

import android.util.Log
import com.isaaclacoba.weeklyfoodplanner.domain.logger.LogLevel
import com.isaaclacoba.weeklyfoodplanner.domain.logger.LoggerTree

class AndroidLogger : LoggerTree {
    override fun log(tag: () -> String, level: LogLevel, msg: () -> String, throwable: Throwable?) {
        when (level) {
            LogLevel.ERROR -> Log.e(tag(), msg(), throwable)
            LogLevel.WARN -> Log.w(tag(), msg())
            LogLevel.INFO -> Log.i(tag(), msg())
            LogLevel.DEBUG -> Log.d(tag(), msg())
            LogLevel.VERBOSE -> Log.v(tag(), msg())
        }
    }
}