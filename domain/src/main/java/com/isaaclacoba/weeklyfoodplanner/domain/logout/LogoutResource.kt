package com.isaaclacoba.weeklyfoodplanner.domain.logout

import com.isaaclacoba.weeklyfoodplanner.domain.repository.RxBaseRepository
import io.reactivex.Observable

class LogoutResource (
    private val logoutRepository: RxBaseRepository<String, ApiLogoutItem>
) {
    fun logout(username: String): Observable<String> {
        return logoutRepository.getByKey(username).map { it.result }
    }
}