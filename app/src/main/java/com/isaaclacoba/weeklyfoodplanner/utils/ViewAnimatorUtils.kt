package com.isaaclacoba.weeklyfoodplanner.utils

import android.view.ViewGroup
import android.animation.ValueAnimator
import android.view.View
import java.util.concurrent.TimeUnit

const public val STANDARD_ANIM_DURATION = 400
public val STANDARD_DURATION_UNIT = TimeUnit.MILLISECONDS

fun modifyViewHeight(heights: Pair<Int, Int>, view: View, duration: Int) {
    val anim = ValueAnimator.ofInt(heights.first, heights.second)
    anim.addUpdateListener { valueAnimator ->
        val `val` = valueAnimator.animatedValue as Int
        val layoutParams = view.getLayoutParams()
        layoutParams.height = `val`
        view.layoutParams = layoutParams
    }
    anim.duration = duration.toLong()
    anim.start()
}