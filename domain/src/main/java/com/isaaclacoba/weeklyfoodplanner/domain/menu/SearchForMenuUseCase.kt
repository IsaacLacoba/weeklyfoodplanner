package com.isaaclacoba.weeklyfoodplanner.domain.menu

import com.isaaclacoba.weeklyfoodplanner.domain.executor.ThreadScheduler
import com.isaaclacoba.weeklyfoodplanner.domain.executor.applyScheduler
import io.reactivex.Observable

interface SearchForMenuUseCase {
    fun execute(menuDate: String) : Observable<MenuItem>
}

class SearchForMenu(
    private val menuResource: MenuResource,
    private val threadScheduler: ThreadScheduler
): SearchForMenuUseCase {
    override fun execute(menuDate: String): Observable<MenuItem> {
        return menuResource.searchForMenu(menuDate)
            .applyScheduler(threadScheduler)
    }
}