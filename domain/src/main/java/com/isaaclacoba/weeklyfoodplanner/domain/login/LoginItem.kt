package com.isaaclacoba.weeklyfoodplanner.domain.login

import com.isaaclacoba.weeklyfoodplanner.domain.repository.Identifiable

data class LoginItem(
    val username: String
)

data class ApiLoginItem(
    val item: LoginItem
) : Identifiable<String> {
    override val key: String
        get() = item.username
}