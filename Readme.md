This project attemps to make it easier the hard task of knowing what to eat each day of the week as the time it helps you to make the shopping list of the ingredients.

You just have to add several recipes, add the ingredients and when you have enough, you can select a set of recipes for each day of the week. 

The app will create the shopping list for you, and will kindly remind each day the food of the next day.