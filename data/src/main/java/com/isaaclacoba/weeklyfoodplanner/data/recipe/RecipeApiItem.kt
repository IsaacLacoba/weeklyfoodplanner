package com.isaaclacoba.weeklyfoodplanner.data.recipe

import com.isaaclacoba.weeklyfoodplanner.domain.menu.IngredientItem
import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeItem

class RecipeApiItem() {
    lateinit var name: String
    lateinit var moment: String
    var imageUrl: String = ""
    lateinit var description: String
    lateinit var ingredients: List<IngredientItem>

    constructor(name: String,
                moment: String,
                imageUrl: String,
                description: String,
                ingredients: List<IngredientItem>): this(){
        this.name = name
        this.moment = moment
        this.imageUrl = imageUrl
        this.description = description
        this.ingredients = ingredients
    }
}

fun RecipeApiItem.toDomain(): RecipeItem =
    RecipeItem (
        name = name,
        moment = moment,
        imageUrl = imageUrl,
        description = moment,
        ingredients = ingredients
    )

fun List<String>.toIngredient(): List<IngredientItem> =
        map { IngredientItem(name = it,
            description = "No description yet",
            imageUrl = "")}

fun RecipeItem.toApiData(): RecipeApiItem =
    RecipeApiItem(name = name,
        moment = moment,
        imageUrl = imageUrl,
        description = moment,
        ingredients = ingredients!!)