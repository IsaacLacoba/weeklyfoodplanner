package com.isaaclacoba.weeklyfoodplanner.customviews

import android.content.Context
import android.support.design.widget.AppBarLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import com.isaaclacoba.weeklyfoodplanner.R
import com.isaaclacoba.weeklyfoodplanner.presentation.menuplan.MenuPlanPresenter
import com.isaaclacoba.weeklyfoodplanner.utils.*
import kotlinx.android.synthetic.main.calendar_layout.view.*
import kotlinx.android.synthetic.main.custom_date_picker.view.*

class CustomDatePicker: AppBarLayout {
    private var isToolbarExpanded : Boolean = false

    private var dayOfMonth: Int = 0
    private var year: Int = 0
    private var month: Int = 0

    private val expandedAnimationHeights: Pair<Int, Int>
        get() = Pair( appBarLayout.minimumHeight, calendar_view.height)

    private val shrunkenAnimationHeights: Pair<Int, Int>
        get() = Pair(calendar_view.height, appBarLayout.minimumHeight)

    constructor(context: Context?) : super(context)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    init {
        LayoutInflater.from(context).inflate(R.layout.custom_date_picker, this, true)
        val todayDateArray = getTodayDateArray()
        dayOfMonth = todayDateArray[2]
        month = todayDateArray[1]
        year = todayDateArray[0]

        configViews()
    }

    private fun configViews() {
        date_btn.text = getTodayDate()
        date_btn.setOnClickListener {
            modifyAppBarHeightInternal()
        }
    }

    fun onDateChange(year: Int, month: Int, dayOfMonth: Int) {
        updateCurrentDate(year, month, dayOfMonth)

        date_btn.text = formatDate(year, month, dayOfMonth)
    }

    private fun updateCurrentDate(year: Int, month: Int, dayOfMonth: Int) {
        this.year = year
        this.month = month
        this.dayOfMonth = dayOfMonth
    }

    private fun getToolbarHeights(): Pair<Int, Int> {
        isToolbarExpanded = !isToolbarExpanded
        return if (isToolbarExpanded) expandedAnimationHeights else shrunkenAnimationHeights
    }

    private fun modifyAppBarHeightInternal(): Boolean {
        modifyViewHeight(getToolbarHeights(), appBarLayout, STANDARD_ANIM_DURATION)
        return true
    }

    fun setOnDateChangeListener(presenter: MenuPlanPresenter) {
        calendar_view.setOnDateChangeListener { _, year, month, dayOfMonth ->
            onDateChanging(presenter, year, month, dayOfMonth)
            modifyAppBarHeightInternal()
        }
    }

    fun setOnClickListeners(presenter: MenuPlanPresenter) {
        next_day_button.setOnClickListener {
            val tomorrowArray = getToday(year, month, dayOfMonth, daysOffset = 1)
            onDateChanging(presenter, year = tomorrowArray[0],
                month = tomorrowArray[1], dayOfMonth = tomorrowArray[2])
            calendar_view.date = dateToLong(year = tomorrowArray[0],
                month = tomorrowArray[1], dayOfMonth = tomorrowArray[2])
        }

        previous_day_btn.setOnClickListener {
            val tomorrowArray = getToday(year, month, dayOfMonth, daysOffset = -1)
            onDateChanging(presenter, tomorrowArray[0], tomorrowArray[1], tomorrowArray[2])
        }
    }

    private fun onDateChanging(
        presenter: MenuPlanPresenter,
        year: Int,
        month: Int,
        dayOfMonth: Int
    ) {
        presenter.onDateChange(year, month, dayOfMonth)
        onDateChange(year, month, dayOfMonth)
    }
}