package com.isaaclacoba.weeklyfoodplanner.domain.menu

class IngredientItem () {
    var name: String = ""
    var imageUrl: String = ""
    var description: String = ""

    constructor(name: String,
                imageUrl: String?,
                description: String): this()
}