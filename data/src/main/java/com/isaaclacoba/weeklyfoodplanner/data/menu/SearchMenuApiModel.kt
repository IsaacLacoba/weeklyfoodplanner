package com.isaaclacoba.weeklyfoodplanner.data.menu

import com.isaaclacoba.weeklyfoodplanner.domain.menu.MenuItem
import com.isaaclacoba.weeklyfoodplanner.domain.menu.SearchMenuItem

//TODO: do a proper implementation for Firebase Model
data class SearchMenuApiModel(
    val query: String,
    val items: MenuItem
)

fun SearchMenuApiModel.toDomain(): SearchMenuItem =
    SearchMenuItem(
        query = query,
        items = items
    )