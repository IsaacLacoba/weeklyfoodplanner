package com.isaaclacoba.weeklyfoodplanner.domain.recipe

import com.isaaclacoba.weeklyfoodplanner.domain.executor.ThreadScheduler
import com.isaaclacoba.weeklyfoodplanner.domain.executor.applyScheduler
import io.reactivex.Completable
import io.reactivex.Observable

interface RecipeUseCase {
    fun addRecipe(recipeItem: RecipeItem): Observable<RecipeItem>
    fun removeRecipe(recipeItem: RecipeItem): Completable
    fun getRecipes(): Observable<List<RecipeItem>>
}

class IRecipeUseCase (
    private val recipeResource: RecipeResource,
    private val threadScheduler: ThreadScheduler
): RecipeUseCase {
    override fun getRecipes(): Observable<List<RecipeItem>> =
        recipeResource.getRecipes().applyScheduler(threadScheduler)

    override fun addRecipe(recipeItem: RecipeItem): Observable<RecipeItem> =
        recipeResource.addRecipe(recipeItem)
            .applyScheduler(threadScheduler)

    override fun removeRecipe(recipeItem: RecipeItem): Completable =
        recipeResource.removeRecipe(recipeItem)
            .applyScheduler(threadScheduler)
}