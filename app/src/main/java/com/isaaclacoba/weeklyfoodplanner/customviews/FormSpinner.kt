package com.isaaclacoba.weeklyfoodplanner.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Spinner
import android.widget.TextView
import com.isaaclacoba.weeklyfoodplanner.R
import com.isaaclacoba.weeklyfoodplanner.utils.loadFormTextView

class FormSpinner(context: Context?, attrs: AttributeSet?): LinearLayout(context, attrs) {

    private val textView: TextView by lazy { rootView.findViewById<TextView>(R.id.title) }
    private val spinner: Spinner by lazy { rootView.findViewById<Spinner>(R.id.spinner) }

    var selectedItem: String = ""
        get() = spinner?.selectedItem?.toString()!!

    init {
        LayoutInflater.from(context).inflate(R.layout.form_spinner, this, true)
        val attributeSpinner = context!!.obtainStyledAttributes(attrs, R.styleable.FormSpinner)
        spinner.prompt = attributeSpinner.getString(R.styleable.FormSpinner_prompt)

        val spinnerAdapter = ArrayAdapter<String>(context,
            android.R.layout.simple_spinner_dropdown_item,
            attributeSpinner.getTextArray(R.styleable.FormSpinner_entries).map { it.toString() })
        spinner.adapter = spinnerAdapter

        loadFormTextView(context, attrs, textView)
    }


}