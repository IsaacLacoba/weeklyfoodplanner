package com.isaaclacoba.weeklyfoodplanner.data.logout

import com.isaaclacoba.weeklyfoodplanner.data.client.FirebaseApiClient
import com.isaaclacoba.weeklyfoodplanner.domain.logout.LogoutApi
import io.reactivex.Observable

class LogoutApiClient (
    private val firebaseApiClient: FirebaseApiClient
) : LogoutApi {
    override fun logout(username: String) =
        firebaseApiClient.logout(username)
}