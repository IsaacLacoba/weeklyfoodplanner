package com.isaaclacoba.weeklyfoodplanner.data.recipe

import com.isaaclacoba.weeklyfoodplanner.domain.reactive.RecipeApi
import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeItem
import com.isaaclacoba.weeklyfoodplanner.domain.repository.RxReadableDataSource
import io.reactivex.Observable

class ReadableRecipeDataSource(
    private val recipeApi: RecipeApi
): RxReadableDataSource<String, RecipeItem> {
    override fun getByKey(key: String): Observable<RecipeItem>  =
        Observable.empty()

    override fun getAll(): Observable<List<RecipeItem>> =
        recipeApi.getRecipes()
}