package com.isaaclacoba.weeklyfoodplanner.injection.modules

import com.isaaclacoba.weeklyfoodplanner.domain.executor.PostExecutionThread
import com.isaaclacoba.weeklyfoodplanner.domain.executor.ThreadExecutor
import com.isaaclacoba.weeklyfoodplanner.domain.executor.ThreadScheduler
import com.isaaclacoba.weeklyfoodplanner.executor.DefaultThreadScheduler
import com.isaaclacoba.weeklyfoodplanner.executor.JobExecutor
import com.isaaclacoba.weeklyfoodplanner.executor.UIPostExecutionThread
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class ExecutorsModule {
    @Provides
    @Singleton
    open fun providesThreadExecutor(): ThreadExecutor = JobExecutor()

    @Provides
    @Singleton
    open fun providesPostExecutionThread(): PostExecutionThread = UIPostExecutionThread()

    @Provides
    @Singleton
    open fun providesThreadScheduler(
        threadExecutor: ThreadExecutor,
        postExecutionThread: PostExecutionThread
    ): ThreadScheduler {
        return DefaultThreadScheduler(
            threadExecutor = threadExecutor,
            postExecutionThread = postExecutionThread
        )
    }
}