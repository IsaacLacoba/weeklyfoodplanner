package com.isaaclacoba.weeklyfoodplanner.domain.login

import com.isaaclacoba.weeklyfoodplanner.domain.executor.ThreadScheduler
import com.isaaclacoba.weeklyfoodplanner.domain.executor.applyScheduler
import io.reactivex.Observable


interface LoginUseCase {
    fun execute(idToken: String): Observable<String>
}

class Login(
    private val loginResource: LoginResource,
    private val threadScheduler: ThreadScheduler
): LoginUseCase {
    override fun execute(idToken: String): Observable<String> {
        return loginResource.login(idToken)
            .applyScheduler(threadScheduler)
    }
}
