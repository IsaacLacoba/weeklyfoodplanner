package com.isaaclacoba.weeklyfoodplanner.utils

import java.util.Calendar

fun formatDate(year: Int, month: Int, day: Int): String {
    return year.toString() + "/" + ( month + 1 )+ "/" + day
}

fun getDateArray(calendar: Calendar): Array<Int> {
    return arrayOf(calendar.get(Calendar.YEAR),
                   calendar.get(Calendar.MONTH),
                   calendar.get(Calendar.DAY_OF_MONTH))
}


fun dateToLong(year: Int, month: Int, dayOfMonth: Int): Long {
    val calendar = Calendar.getInstance()

    calendar.set(Calendar.YEAR, year)
    calendar.set(Calendar.MONTH, month)
    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

    //This return a Date Object which returns milliseconds from Epoch
    return calendar.time.time
}

fun getToday(year: Int, month: Int,
             dayOfTheMonth: Int,
             daysOffset: Int = 0
): Array<Int> {
    val calendar = Calendar.getInstance()
    calendar.set(Calendar.YEAR, year)
    calendar.set(Calendar.MONTH, month)
    calendar.set(Calendar.DAY_OF_MONTH, dayOfTheMonth)
    calendar.add(Calendar.DAY_OF_MONTH, daysOffset)

    return getDateArray(calendar)
}

fun getToday(daysOffset: Int): Array<Int> {
    val calendar = Calendar.getInstance()

    calendar.add(Calendar.DAY_OF_MONTH, daysOffset)

    return getDateArray(calendar)
}

fun getTodayDate(): String {
    val todayArray = getToday(daysOffset = 0)
    return formatDate(todayArray[0], todayArray[1], todayArray[2])
}

fun getTodayDateArray(): Array<Int> {
    return getToday(daysOffset = 0)
}