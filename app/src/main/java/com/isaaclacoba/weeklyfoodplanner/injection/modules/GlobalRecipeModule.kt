package com.isaaclacoba.weeklyfoodplanner.injection.modules

import com.isaaclacoba.weeklyfoodplanner.data.recipe.ReadableRecipeDataSource
import com.isaaclacoba.weeklyfoodplanner.data.recipe.WritableRecipeDataSource
import com.isaaclacoba.weeklyfoodplanner.data.repository.CachePolicyTtl
import com.isaaclacoba.weeklyfoodplanner.data.repository.RxInMemoryCacheDataSource
import com.isaaclacoba.weeklyfoodplanner.domain.reactive.RecipeApi
import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeItem
import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeResource
import com.isaaclacoba.weeklyfoodplanner.domain.repository.RxBaseRepository
import com.isaaclacoba.weeklyfoodplanner.domain.repository.TimeProvider

import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module( includes = [TimeProviderModule::class, FirebaseModule::class])
open class GlobalRecipeModule {
    companion object {
        private const val REPO_VERSION: Int = 1
    }

    @Provides
    fun provideRecipeResource(
        recipeRepository: RxBaseRepository<String, RecipeItem>
    ): RecipeResource = RecipeResource(recipeRepository)

    @Provides
    fun provideRecipeRepository(
        searchRecipeInMemoryDataSource: RxInMemoryCacheDataSource<String, RecipeItem>,
        writableRecipeDataSource: WritableRecipeDataSource,
        readableDataSource: ReadableRecipeDataSource
    ): RxBaseRepository<String, RecipeItem> =
        RxBaseRepository(
            rxCacheableDataSources = listOf(searchRecipeInMemoryDataSource),
            rxWriteableDataSources = listOf(writableRecipeDataSource),
            rxReadableDataSources = listOf(readableDataSource)
        )

    @Provides
    @Reusable
    fun provideWritableRecipeDataSource(
        recipeApi: RecipeApi
    ): WritableRecipeDataSource =
            WritableRecipeDataSource(recipeApi)

    @Provides
    @Reusable
    fun provideReadableRecipeDataSource(
        recipeApi: RecipeApi
    ): ReadableRecipeDataSource =
        ReadableRecipeDataSource(recipeApi)

    @Provides
    @Reusable
    fun provideRecipeInMemoryDataSource(
        timeProvider: TimeProvider
    ): RxInMemoryCacheDataSource<String, RecipeItem> =
            RxInMemoryCacheDataSource(
                version = REPO_VERSION,
                timeProvider = timeProvider,
                policies = listOf(
                    CachePolicyTtl.oneMinute(timeProvider = timeProvider)
                )
            )
}