package com.isaaclacoba.weeklyfoodplanner.presentation.menuplan

import com.isaaclacoba.weeklyfoodplanner.base.BaseView
import com.isaaclacoba.weeklyfoodplanner.domain.menu.*
import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeItem

interface MenuPlanListView: BaseView {
    fun showDayMenu(menuList: List<MenuUI>)
    fun showMenuError()
    fun logout()
    fun backToLoginView()
}

interface MenuUI

data class MenuItemUI(
    val name: String,
    val moment: String,
    val imageUrl: String?,
    val description: String
): MenuUI

fun MenuItem.toUI() : List<MenuItemUI> {
    return recipeList!!.toUI()
}

fun List<RecipeItem>.toUI() : List<MenuItemUI> {
    return map { it.toUI() }
}

fun RecipeItem.toUI() : MenuItemUI {
    return MenuItemUI (
        name = name,
        moment = moment,
        imageUrl = imageUrl,
        description = description)
}