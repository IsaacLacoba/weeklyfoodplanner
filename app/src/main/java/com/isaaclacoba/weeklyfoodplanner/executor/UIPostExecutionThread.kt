package com.isaaclacoba.weeklyfoodplanner.executor

import com.isaaclacoba.weeklyfoodplanner.domain.executor.PostExecutionThread
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

class UIPostExecutionThread : PostExecutionThread {
    override fun getScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}