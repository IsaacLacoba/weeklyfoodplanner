package com.isaaclacoba.weeklyfoodplanner.domain.recipe

import com.isaaclacoba.weeklyfoodplanner.domain.menu.IngredientItem
import com.isaaclacoba.weeklyfoodplanner.domain.repository.Identifiable

data class RecipeItem(
    val name: String,
    var moment: String,
    var imageUrl: String,
    val description: String,
    val ingredients: List<IngredientItem>?
): Identifiable<String> {
    override val key: String
    get() = name
}