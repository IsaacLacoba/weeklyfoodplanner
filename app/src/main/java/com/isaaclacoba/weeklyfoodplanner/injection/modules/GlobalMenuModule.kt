package com.isaaclacoba.weeklyfoodplanner.injection.modules

import com.isaaclacoba.weeklyfoodplanner.data.logout.LogoutApiDataSource
import com.isaaclacoba.weeklyfoodplanner.data.menu.SearchMenuApiDataSource
import com.isaaclacoba.weeklyfoodplanner.data.repository.CachePolicyTtl
import com.isaaclacoba.weeklyfoodplanner.data.repository.RxInMemoryCacheDataSource
import com.isaaclacoba.weeklyfoodplanner.domain.logout.ApiLogoutItem
import com.isaaclacoba.weeklyfoodplanner.domain.logout.LogoutApi
import com.isaaclacoba.weeklyfoodplanner.domain.logout.LogoutResource
import com.isaaclacoba.weeklyfoodplanner.domain.menu.MenuApi
import com.isaaclacoba.weeklyfoodplanner.domain.menu.MenuResource
import com.isaaclacoba.weeklyfoodplanner.domain.menu.SearchMenuItem
import com.isaaclacoba.weeklyfoodplanner.domain.repository.RxBaseRepository
import com.isaaclacoba.weeklyfoodplanner.domain.repository.TimeProvider
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module(includes = [TimeProviderModule::class, FirebaseModule::class])
open class GlobalMenuModule {
    companion object {
        private const val REPO_VERSION: Int = 1
    }

    @Provides
    fun provideMenuResource(
        searchMenuRepository: RxBaseRepository<String, SearchMenuItem>
    ): MenuResource {
        return MenuResource(searchMenuRepository)
    }

    @Provides
    fun provideSearchMenuRepository(
        searchMenuInMemoryDataSource: RxInMemoryCacheDataSource<String, SearchMenuItem>,
        searchMenuApiDataSource: SearchMenuApiDataSource
    ): RxBaseRepository<String, SearchMenuItem> =
        RxBaseRepository(
            rxCacheableDataSources = listOf(searchMenuInMemoryDataSource),
            rxWriteableDataSources = listOf(),
            rxReadableDataSources = listOf(searchMenuApiDataSource)
        )

    @Provides
    @Reusable
    fun providesSearchMenuApiDataSource(
        menuApi: MenuApi
    ): SearchMenuApiDataSource =
        SearchMenuApiDataSource(menuApi)

    @Provides
    @Reusable
    fun providesSearchMenuInMemoryDataSource(
        timeProvider: TimeProvider
    ): RxInMemoryCacheDataSource<String, SearchMenuItem> =
        RxInMemoryCacheDataSource(
            version = REPO_VERSION,
            timeProvider = timeProvider,
            policies = listOf(
                CachePolicyTtl.oneMinute(timeProvider = timeProvider)
            )
        )

    @Provides
    fun providesLogoutResource(
        logoutRepository: RxBaseRepository<String, ApiLogoutItem>): LogoutResource {
        return LogoutResource(logoutRepository)
    }

    @Provides
    fun providesLogoutRepository(
       logoutInMemoryDataSource: RxInMemoryCacheDataSource<String, ApiLogoutItem>,
       logoutApiDataSource: LogoutApiDataSource
    ): RxBaseRepository<String, ApiLogoutItem> =
        RxBaseRepository(
            rxCacheableDataSources = listOf(logoutInMemoryDataSource),
            rxWriteableDataSources = listOf(),
            rxReadableDataSources = listOf(logoutApiDataSource)
        )

    @Provides
    @Reusable
    fun providesLogoutApiDataSource(
        logoutApi: LogoutApi
    ): LogoutApiDataSource =
        LogoutApiDataSource(logoutApi)

    @Provides
    @Reusable
    fun providesLogoutInMemoryDataSource(
        timeProvider: TimeProvider
    ): RxInMemoryCacheDataSource<String, ApiLogoutItem> =
            RxInMemoryCacheDataSource(
                version = REPO_VERSION,
                timeProvider = timeProvider,
                policies = listOf(
                    CachePolicyTtl.oneMinute(timeProvider = timeProvider)
                )
            )
}