package com.isaaclacoba.weeklyfoodplanner.presentation.recipe.utils

import com.isaaclacoba.weeklyfoodplanner.domain.logger.Logger
import com.isaaclacoba.weeklyfoodplanner.presentation.recipe.RecipeUI
import com.pedrogomez.renderers.RVRendererAdapter

fun RVRendererAdapter<RecipeUI>.addOrUpdate(itemToInsert: RecipeUI): Boolean {
    //Todo: this won't be necessary when we have a proper local cache
    repeat(itemCount) {
        Logger.d { "trying to insert not duplicated element" }
        val currentItem = getItem(it)
        if (itemToInsert.name == currentItem.name) {
            return updateRecipe(currentItem, itemToInsert, position = it)
        }
    }
    return add(itemToInsert)
}

fun RVRendererAdapter<RecipeUI>.updateRecipe(itemToUpdate: RecipeUI, itemToCopy: RecipeUI, position: Int = -1): Boolean {
    itemToUpdate.update(itemToCopy)
    notifyDataSetChanged()
    return true
}