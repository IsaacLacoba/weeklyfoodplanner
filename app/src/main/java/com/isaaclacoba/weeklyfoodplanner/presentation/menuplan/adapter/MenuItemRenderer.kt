package com.isaaclacoba.weeklyfoodplanner.presentation.menuplan.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.isaaclacoba.weeklyfoodplanner.R
import com.isaaclacoba.weeklyfoodplanner.presentation.menuplan.MenuItemUI
import com.isaaclacoba.weeklyfoodplanner.utils.loadImage
import com.pedrogomez.renderers.Renderer

class MenuItemRenderer(
    private  val onClick: (item: MenuItemUI) -> Unit = {}
) : Renderer<MenuItemUI>() {
    lateinit var recipeName: TextView
    lateinit var recipeMoment: TextView
    lateinit var descriptionName: TextView
    lateinit var recipeImage: ImageView


    override fun inflate(inflater: LayoutInflater?, parent: ViewGroup?): View =
        inflater!!.inflate(R.layout.menulist_item, parent!!, false)

    override fun setUpView(rootView: View?) {
        recipeName = rootView!!.findViewById<TextView>(R.id.name)
        recipeMoment = rootView!!.findViewById<TextView>(R.id.moment)
        descriptionName = rootView!!.findViewById<TextView>(R.id.description)
        recipeImage = rootView!!.findViewById<ImageView>(R.id.image)
    }

    override fun hookListeners(rootView: View?) {
        rootView!!.setOnClickListener{
            onClick.invoke(content)
        }
    }

    override fun render() {
        with(rootView) {
            recipeName?.text = content.name
            recipeMoment?.text = content.moment
            descriptionName?.text = content.description
            recipeImage?.loadImage(content.imageUrl)
        }
    }
}