package com.isaaclacoba.weeklyfoodplanner.data.recipe

import com.isaaclacoba.weeklyfoodplanner.data.client.FirebaseApiClient
import com.isaaclacoba.weeklyfoodplanner.domain.reactive.RecipeApi
import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeItem
import io.reactivex.Completable
import io.reactivex.Observable

class RecipeApiClient (
    private val firebaseApiClient: FirebaseApiClient
) : RecipeApi {
    override fun getRecipes(): Observable<List<RecipeItem>> =
        firebaseApiClient.getRecipes().map { recipeApiList->
            recipeApiList.map {
                    recipeApiItem ->
                recipeApiItem.toDomain()
            } }

    override fun addRecipe(recipe: RecipeItem): Observable<RecipeItem> =
        firebaseApiClient.addRecipe(recipe)

    override fun deleteRecipe(key: String): Completable =
        firebaseApiClient.deleteRecipe(key)
}