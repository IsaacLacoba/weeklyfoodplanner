package com.isaaclacoba.weeklyfoodplanner.domain.recipe

import com.isaaclacoba.weeklyfoodplanner.domain.repository.RxBaseRepository
import io.reactivex.Observable

class RecipeResource(
    private val recipeRepository: RxBaseRepository<String, RecipeItem>
) {
    fun addRecipe(recipe: RecipeItem): Observable<RecipeItem> =
        recipeRepository.addOrUpdate(recipe)

    fun removeRecipe(recipe: RecipeItem) =
        recipeRepository.deleteByKey(recipe.key)

    fun getRecipes(): Observable<List<RecipeItem>> =
            recipeRepository.getAll()
 }