package com.isaaclacoba.weeklyfoodplanner.injection.modules

import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.isaaclacoba.weeklyfoodplanner.data.client.FirebaseApiClient
import com.isaaclacoba.weeklyfoodplanner.data.login.LoginApiClient
import com.isaaclacoba.weeklyfoodplanner.data.logout.LogoutApiClient
import com.isaaclacoba.weeklyfoodplanner.data.menu.MenuApiClient
import com.isaaclacoba.weeklyfoodplanner.data.recipe.RecipeApiClient
import com.isaaclacoba.weeklyfoodplanner.domain.login.LoginApi
import com.isaaclacoba.weeklyfoodplanner.domain.logout.LogoutApi
import com.isaaclacoba.weeklyfoodplanner.domain.menu.MenuApi
import com.isaaclacoba.weeklyfoodplanner.domain.reactive.RecipeApi
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
open class FirebaseModule {
    @Provides
    fun providesLoginApi( firebaseApiClient: FirebaseApiClient) : LoginApi =
        LoginApiClient(firebaseApiClient)

    @Provides
    fun providesLogoutApi( firebaseApiClient: FirebaseApiClient) : LogoutApi =
        LogoutApiClient(firebaseApiClient)

    @Provides
    fun providesMenuApi(firebaseApiClient: FirebaseApiClient) : MenuApi =
        MenuApiClient(firebaseApiClient)

    @Provides
    fun provideRecipeApi(firebaseApiClient: FirebaseApiClient) : RecipeApi =
        RecipeApiClient(firebaseApiClient)

    @Provides
    @Reusable
    fun providesFirebaseApiClient(
        firebaseAuth: FirebaseAuth,
        firebaseDatabase: FirebaseDatabase
    ) : FirebaseApiClient = FirebaseApiClient(firebaseAuth, firebaseDatabase)

    @Provides
    @Reusable
    fun providesFirebaseAuth(): FirebaseAuth = FirebaseAuth.getInstance()

    @Provides
    @Reusable
    fun providesFirebaseDatabase() : FirebaseDatabase {
        val firebaseDatabase = FirebaseDatabase.getInstance()
        firebaseDatabase.setPersistenceEnabled(true)
        return firebaseDatabase
    }


    @Provides
    fun providesGoogleSignInOptions(): GoogleSignInOptions =
        GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("777127747879-q80l4agdbcqbe578q9j2efhvdomb4pf4.apps.googleusercontent.com")
            .requestEmail()
            .build()
}
