package com.isaaclacoba.weeklyfoodplanner.presentation.recipe

import com.isaaclacoba.weeklyfoodplanner.R
import com.isaaclacoba.weeklyfoodplanner.base.BasePresenter
import com.isaaclacoba.weeklyfoodplanner.domain.logger.Logger
import com.isaaclacoba.weeklyfoodplanner.domain.reactive.addDisposableTo
import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeItem
import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeUseCase
import io.reactivex.rxkotlin.subscribeBy

class RecipePresenter(
    private val recipeUseCase: RecipeUseCase
): BasePresenter<RecipeView>() {
    private val LOG_TAG = "RecipePresenter"

    fun getRecipes() {
        recipeUseCase.getRecipes().subscribeBy (
            onNext = {
                val recipeUIList = it.map { recipe -> recipe.toUI() }
                getView()?.showRecipeList(recipeUIList)
            },
            onError = {
                Logger.e { "Upsie! an error was encountered" }
            }
        ).addDisposableTo(disposeBag)
    }

    fun addRecipe(recipe: RecipeUI) {
        Logger.d { "$LOG_TAG addRecipe" }
        recipeUseCase.addRecipe((recipe as RecipeItemUI).toDomain()).subscribeBy(
            onNext = {

            },
            onError = {

            }
        ).addDisposableTo(disposeBag)
    }

    fun removeRecipe(recipe: RecipeUI) {
        recipeUseCase.removeRecipe((recipe as RecipeItemUI).toDomain()).subscribeBy(
            onError = {

            }
        ).addDisposableTo(disposeBag)
    }

    fun loadRecipes() {
        Logger.d { "$LOG_TAG loadRecipes" }
    }
}