package com.isaaclacoba.weeklyfoodplanner.data.recipe

import com.isaaclacoba.weeklyfoodplanner.domain.logger.Logger
import com.isaaclacoba.weeklyfoodplanner.domain.reactive.RecipeApi
import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeItem
import com.isaaclacoba.weeklyfoodplanner.domain.repository.RxReadableDataSource
import com.isaaclacoba.weeklyfoodplanner.domain.repository.RxWriteableDataSource
import io.reactivex.Completable
import io.reactivex.Observable

class WritableRecipeDataSource(
    private val recipeApi: RecipeApi
): RxWriteableDataSource<String, RecipeItem> {

    override fun addOrUpdate(value: RecipeItem): Observable<RecipeItem> {
        Logger.d { "Baia Baia papaia" }
        return recipeApi.addRecipe(value)
    }

    override fun replaceAll(values: List<RecipeItem>): Observable<List<RecipeItem>> =
        Observable.empty()

    override fun deleteByKey(key: String): Completable =
        recipeApi.deleteRecipe(key)

    override fun deleteAll(): Completable =
        Completable.complete()
}