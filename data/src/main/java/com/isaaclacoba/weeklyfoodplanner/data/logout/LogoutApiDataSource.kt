package com.isaaclacoba.weeklyfoodplanner.data.logout

import com.isaaclacoba.weeklyfoodplanner.domain.login.ApiLoginItem
import com.isaaclacoba.weeklyfoodplanner.domain.login.LoginItem
import com.isaaclacoba.weeklyfoodplanner.domain.logout.ApiLogoutItem
import com.isaaclacoba.weeklyfoodplanner.domain.logout.LogoutApi
import com.isaaclacoba.weeklyfoodplanner.domain.repository.RxReadableDataSource
import io.reactivex.Observable


class LogoutApiDataSource(
    private val logoutApi: LogoutApi
): RxReadableDataSource<String, ApiLogoutItem> {
    override fun getAll(): Observable<List<ApiLogoutItem>> =
        Observable.empty()

    override fun getByKey(key: String): Observable<ApiLogoutItem> =
        logoutApi.logout(key).map { it }
}