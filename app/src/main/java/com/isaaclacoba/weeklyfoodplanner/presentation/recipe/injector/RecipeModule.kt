package com.isaaclacoba.weeklyfoodplanner.presentation.recipe.injector

import android.support.v7.app.AppCompatActivity
import com.isaaclacoba.weeklyfoodplanner.domain.executor.ThreadScheduler
import com.isaaclacoba.weeklyfoodplanner.domain.recipe.IRecipeUseCase
import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeResource
import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeUseCase
import com.isaaclacoba.weeklyfoodplanner.injection.PerActivity
import com.isaaclacoba.weeklyfoodplanner.injection.component.ActivityComponent
import com.isaaclacoba.weeklyfoodplanner.injection.component.ApplicationComponent
import com.isaaclacoba.weeklyfoodplanner.injection.modules.ActivityModule
import com.isaaclacoba.weeklyfoodplanner.injection.modules.FirebaseModule
import com.isaaclacoba.weeklyfoodplanner.injection.modules.TimeProviderModule
import com.isaaclacoba.weeklyfoodplanner.presentation.recipe.RecipeActivity
import com.isaaclacoba.weeklyfoodplanner.presentation.recipe.RecipePresenter
import dagger.Component
import dagger.Module
import dagger.Provides

@Module(includes = [TimeProviderModule::class, FirebaseModule::class])
class RecipeModule(private val activity: AppCompatActivity): ActivityModule {

    @Provides
    fun provideRecipePresenter(recipeUseCase: RecipeUseCase) : RecipePresenter =
        RecipePresenter(recipeUseCase)

    @Provides
    fun provideRecipeUseCase(
        recipeResource: RecipeResource,
        threadScheduler: ThreadScheduler
    ): RecipeUseCase =
        IRecipeUseCase(
            recipeResource = recipeResource,
            threadScheduler = threadScheduler
        )
}

@PerActivity
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [RecipeModule::class]
)
interface RecipeComponent: ActivityComponent {
    fun inject(activity: RecipeActivity)
}
