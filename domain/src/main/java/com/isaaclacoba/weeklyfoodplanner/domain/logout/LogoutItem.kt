package com.isaaclacoba.weeklyfoodplanner.domain.logout

import com.isaaclacoba.weeklyfoodplanner.domain.repository.Identifiable

data class LogoutItem(
    val username: String
)

data class ApiLogoutItem(
    val item: LogoutItem,
    val result: String
) : Identifiable<String> {
    override val key: String
        get() = result
}