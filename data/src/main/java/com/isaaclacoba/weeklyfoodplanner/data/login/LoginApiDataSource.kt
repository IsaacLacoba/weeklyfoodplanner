package com.isaaclacoba.weeklyfoodplanner.data.login

import com.isaaclacoba.weeklyfoodplanner.domain.login.ApiLoginItem
import com.isaaclacoba.weeklyfoodplanner.domain.login.LoginApi
import com.isaaclacoba.weeklyfoodplanner.domain.login.LoginItem
import com.isaaclacoba.weeklyfoodplanner.domain.repository.RxReadableDataSource
import io.reactivex.Observable

class LoginApiDataSource(
    private val loginApi: LoginApi
): RxReadableDataSource<String, ApiLoginItem> {
    override fun getAll(): Observable<List<ApiLoginItem>> =
        Observable.empty<List<ApiLoginItem>>()

    override fun getByKey(key: String): Observable<ApiLoginItem> =
        loginApi.login(key).map { ApiLoginItem(LoginItem(it)) }
}