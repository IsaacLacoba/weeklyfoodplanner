package com.isaaclacoba.weeklyfoodplanner.utils

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import com.isaaclacoba.weeklyfoodplanner.R

fun loadFormTextView(context: Context?, attrs: AttributeSet?, textView: TextView) {
    val attributesTextView = context!!.obtainStyledAttributes(attrs, R.styleable.TextViewStyle)
    textView.text = attributesTextView.getString(R.styleable.TextViewStyle_title)
}