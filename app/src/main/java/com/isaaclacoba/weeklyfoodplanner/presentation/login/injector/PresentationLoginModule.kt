package com.isaaclacoba.weeklyfoodplanner.presentation.login.injector

import android.support.v7.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.isaaclacoba.weeklyfoodplanner.domain.executor.ThreadScheduler
import com.isaaclacoba.weeklyfoodplanner.domain.login.Login
import com.isaaclacoba.weeklyfoodplanner.domain.login.LoginResource
import com.isaaclacoba.weeklyfoodplanner.domain.login.LoginUseCase
import com.isaaclacoba.weeklyfoodplanner.injection.PerActivity
import com.isaaclacoba.weeklyfoodplanner.injection.component.ActivityComponent
import com.isaaclacoba.weeklyfoodplanner.injection.component.ApplicationComponent
import com.isaaclacoba.weeklyfoodplanner.injection.modules.ActivityModule
import com.isaaclacoba.weeklyfoodplanner.injection.modules.FirebaseModule
import com.isaaclacoba.weeklyfoodplanner.injection.modules.TimeProviderModule
import com.isaaclacoba.weeklyfoodplanner.presentation.login.LoginActivity
import com.isaaclacoba.weeklyfoodplanner.presentation.login.LoginPresenter
import dagger.Component
import dagger.Module
import dagger.Provides

@Module(includes = [TimeProviderModule::class, FirebaseModule::class])
open class PresentationLoginModule(private val activity: AppCompatActivity) : ActivityModule {
    @Provides
    fun providesLoginPresenter(loginUseCase: LoginUseCase): LoginPresenter {
        return LoginPresenter(loginUseCase)
    }

    @Provides
    fun providesLoginUseCase(
        loginResource: LoginResource,
        threadScheduler: ThreadScheduler
    ): LoginUseCase {
        return Login(
            loginResource,
            threadScheduler
        )
    }

    @Provides
    fun providesGoogleSignInClient(signInOptions: GoogleSignInOptions): GoogleSignInClient =
        GoogleSignIn.getClient(activity, signInOptions)
}

@PerActivity
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [PresentationLoginModule::class]
)
interface LoginComponent: ActivityComponent {
    fun inject(activity: LoginActivity)
}