package com.isaaclacoba.weeklyfoodplanner.data.login

import com.isaaclacoba.weeklyfoodplanner.data.client.FirebaseApiClient
import com.isaaclacoba.weeklyfoodplanner.domain.login.ApiLoginItem
import com.isaaclacoba.weeklyfoodplanner.domain.login.LoginApi
import io.reactivex.Observable

class LoginApiClient (
    private val firebaseApiClient: FirebaseApiClient
) : LoginApi {
    override fun login(idToken: String): Observable<String> =
        firebaseApiClient.signInWithGoogle(idToken)
}