package com.isaaclacoba.weeklyfoodplanner.presentation.login

import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.isaaclacoba.weeklyfoodplanner.base.BasePresenter
import com.isaaclacoba.weeklyfoodplanner.domain.logger.Logger
import com.isaaclacoba.weeklyfoodplanner.domain.login.LoginUseCase
import com.isaaclacoba.weeklyfoodplanner.domain.reactive.addDisposableTo
import io.reactivex.rxkotlin.subscribeBy

class LoginPresenter(
    private val loginUseCase: LoginUseCase
): BasePresenter<LoginView>() {

    private val TAG: String = "LoginPresenter"

    fun login(account: GoogleSignInAccount) {
        loginUseCase.execute(account.idToken!!).subscribeBy(
            onNext = {
                Logger.d {"$TAG signInWithCredential:success"}
                getView()?.loginCompleted()
            },
            onError = {
                Logger.d{"$TAG signInWithCredential:failure: $it.exception"}

                getView()?.showLoginError(it.toString())
            }
        ).addDisposableTo(disposeBag)
    }
}