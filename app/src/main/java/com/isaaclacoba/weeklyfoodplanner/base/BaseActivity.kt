package com.isaaclacoba.weeklyfoodplanner.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.isaaclacoba.weeklyfoodplanner.lifecycle.PresenterLifeCycleLinker
import com.isaaclacoba.weeklyfoodplanner.utils.then
import com.isaaclacoba.weeklyfoodplanner.R

abstract class BaseActivity : AppCompatActivity(), BaseView {

    abstract val layoutRes: Int

    abstract val hasSupportActionBar: Boolean
    abstract val hasDisplayHomeAsUpEnabled: Boolean

    private val presenterLifeCycleLinker = PresenterLifeCycleLinker()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initInjector()
        setContentView(layoutRes)
        setupViews()
        presenterLifeCycleLinker.onCreate(this)
    }

    abstract fun initInjector()

    override fun onResume() {
        super.onResume()
        presenterLifeCycleLinker.onResume(this)
    }

    override fun onPause() {
        super.onPause()
        presenterLifeCycleLinker.onPause()
    }

    override fun onDestroy() {
        presenterLifeCycleLinker.onDestroy()
        super.onDestroy()
    }

    open fun setupViews() {
        hasSupportActionBar then { setSupportActionBar(findViewById(R.id.toolbar)) }
        hasDisplayHomeAsUpEnabled then {
            supportActionBar?.setDisplayHomeAsUpEnabled(true);
            supportActionBar?.setDisplayShowHomeEnabled(true);
        }
    }

    override fun onLowMemory() {
        presenterLifeCycleLinker.onLowMemory()
        super.onLowMemory()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}