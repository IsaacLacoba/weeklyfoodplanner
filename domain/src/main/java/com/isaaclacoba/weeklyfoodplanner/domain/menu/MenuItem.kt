package com.isaaclacoba.weeklyfoodplanner.domain.menu

import com.isaaclacoba.weeklyfoodplanner.domain.recipe.RecipeItem
import com.isaaclacoba.weeklyfoodplanner.domain.repository.Identifiable


data class MenuItem(
    val name: String,
    val date: String,
    val imageUrl: String?,
    val description: String?,
    val recipeList: List<RecipeItem>?
)

data class SearchMenuItem(
    val query: String,
    val items: MenuItem
): Identifiable<String> {
    override val key: String
        get() = query
}