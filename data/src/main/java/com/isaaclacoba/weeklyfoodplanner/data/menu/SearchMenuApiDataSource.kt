package com.isaaclacoba.weeklyfoodplanner.data.menu

import com.isaaclacoba.weeklyfoodplanner.domain.menu.MenuApi
import com.isaaclacoba.weeklyfoodplanner.domain.menu.SearchMenuItem
import com.isaaclacoba.weeklyfoodplanner.domain.repository.RxReadableDataSource
import com.isaaclacoba.weeklyfoodplanner.domain.repository.RxWriteableDataSource
import io.reactivex.Completable
import io.reactivex.Observable

class SearchMenuApiDataSource(
    private val menuApi: MenuApi
) : RxReadableDataSource<String, SearchMenuItem> {
    override fun getAll(): Observable<List<SearchMenuItem>> =
        Observable.empty()


    override fun getByKey(key: String): Observable<SearchMenuItem> =
        menuApi.searchForMenu(key)
}