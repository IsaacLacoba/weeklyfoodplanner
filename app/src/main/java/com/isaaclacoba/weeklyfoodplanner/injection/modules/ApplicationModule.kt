package com.isaaclacoba.weeklyfoodplanner.injection.modules

import android.app.Application
import android.content.Context
import com.isaaclacoba.weeklyfoodplanner.presentation.login.injector.PresentationLoginModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module (
    includes = [ExecutorsModule::class,
        PresentationLoginModule::class,
        GlobalMenuModule::class,
        GlobalLoginModule::class,
        GlobalRecipeModule::class]
)
class ApplicationModule(private val application: Application) {
    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application
}