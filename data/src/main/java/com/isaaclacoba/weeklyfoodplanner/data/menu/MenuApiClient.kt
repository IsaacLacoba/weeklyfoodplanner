package com.isaaclacoba.weeklyfoodplanner.data.menu

import com.isaaclacoba.weeklyfoodplanner.data.client.FirebaseApiClient
import com.isaaclacoba.weeklyfoodplanner.domain.menu.MenuApi
import com.isaaclacoba.weeklyfoodplanner.domain.menu.SearchMenuItem
import io.reactivex.Observable


class MenuApiClient(
    private val firebaseApiClient: FirebaseApiClient
): MenuApi {
    override fun searchForMenu(date: String): Observable<SearchMenuItem> {
        return firebaseApiClient.getMenu(
            date = date
        ).map{ it.toDomain() }
    }
}
