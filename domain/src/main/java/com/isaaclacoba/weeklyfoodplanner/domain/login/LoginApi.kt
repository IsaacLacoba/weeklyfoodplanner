package com.isaaclacoba.weeklyfoodplanner.domain.login

import io.reactivex.Observable

interface LoginApi {
    fun login(idToken: String): Observable<String>
}