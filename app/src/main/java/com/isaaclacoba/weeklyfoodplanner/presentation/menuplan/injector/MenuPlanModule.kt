package com.isaaclacoba.weeklyfoodplanner.presentation.menuplan.injector

import android.support.v7.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.isaaclacoba.weeklyfoodplanner.domain.executor.ThreadScheduler
import com.isaaclacoba.weeklyfoodplanner.domain.logout.LogoutResource
import com.isaaclacoba.weeklyfoodplanner.domain.logout.LogoutUseCase
import com.isaaclacoba.weeklyfoodplanner.domain.menu.MenuResource
import com.isaaclacoba.weeklyfoodplanner.domain.menu.SearchForMenu
import com.isaaclacoba.weeklyfoodplanner.domain.menu.SearchForMenuUseCase
import com.isaaclacoba.weeklyfoodplanner.injection.PerActivity
import com.isaaclacoba.weeklyfoodplanner.injection.component.ActivityComponent
import com.isaaclacoba.weeklyfoodplanner.injection.component.ApplicationComponent
import com.isaaclacoba.weeklyfoodplanner.injection.modules.ActivityModule
import com.isaaclacoba.weeklyfoodplanner.injection.modules.FirebaseModule
import com.isaaclacoba.weeklyfoodplanner.injection.modules.TimeProviderModule
import com.isaaclacoba.weeklyfoodplanner.presentation.menuplan.MenuPlanActivity
import com.isaaclacoba.weeklyfoodplanner.presentation.menuplan.MenuPlanPresenter
import dagger.Component
import dagger.Module
import dagger.Provides

@Module(includes = [TimeProviderModule::class, FirebaseModule::class])
open class MenuPlanModule(private val activity: AppCompatActivity) : ActivityModule {
    @Provides
    fun provideMenuPlanPresenter(searchForMenuUseCase: SearchForMenuUseCase,
                                 logoutUseCase: LogoutUseCase): MenuPlanPresenter =
        MenuPlanPresenter(searchForMenuUseCase, logoutUseCase)

    @Provides
    fun provideSearchForMenuUseCase(
        menuResource: MenuResource,
        threadScheduler: ThreadScheduler
    ): SearchForMenuUseCase =
        SearchForMenu(
            menuResource = menuResource,
            threadScheduler = threadScheduler
        )

    @Provides
    fun provideLogoutUseCase(
        logoutResource: LogoutResource,
        threadScheduler: ThreadScheduler
    ): LogoutUseCase =
         LogoutUseCase(
             logoutResource = logoutResource,
             threadScheduler = threadScheduler
         )

    @Provides
    fun providesGoogleSignInClient(signInOptions: GoogleSignInOptions): GoogleSignInClient =
        GoogleSignIn.getClient(activity, signInOptions)
}

@PerActivity
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [MenuPlanModule::class]
)
interface MenuPlanComponent: ActivityComponent {
    fun inject(activity: MenuPlanActivity)
}
