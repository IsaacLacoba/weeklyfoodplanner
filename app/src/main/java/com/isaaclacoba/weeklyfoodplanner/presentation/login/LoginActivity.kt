package com.isaaclacoba.weeklyfoodplanner.presentation.login

import android.content.Intent
import android.os.Bundle
import android.widget.ProgressBar
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.isaaclacoba.weeklyfoodplanner.R
import com.isaaclacoba.weeklyfoodplanner.WFPApplication
import com.isaaclacoba.weeklyfoodplanner.base.BaseActivity
import com.isaaclacoba.weeklyfoodplanner.domain.logger.Logger
import com.isaaclacoba.weeklyfoodplanner.lifecycle.Presenter
import com.isaaclacoba.weeklyfoodplanner.presentation.login.injector.DaggerLoginComponent
import com.isaaclacoba.weeklyfoodplanner.presentation.login.injector.PresentationLoginModule
import com.isaaclacoba.weeklyfoodplanner.utils.RC_SIGN_IN
import javax.inject.Inject
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import com.isaaclacoba.weeklyfoodplanner.presentation.menuplan.MenuPlanActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class LoginActivity: BaseActivity(), LoginView {
    override val layoutRes: Int = R.layout.login_layout
    override val hasSupportActionBar: Boolean = false
    override val hasDisplayHomeAsUpEnabled = false

    private val TAG: String = "LoginActivity"
    @Presenter
    @Inject
    lateinit var presenter: LoginPresenter

    @Inject
    lateinit var googleSignIn: GoogleSignInClient

    private val progressBar: ProgressBar by lazy {
        findViewById<ProgressBar>(R.id.progress_circular)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GlobalScope.launch {
            delay(700)
            val lastSignedInAccount = GoogleSignIn.getLastSignedInAccount(applicationContext)
            if (lastSignedInAccount != null) {
                loginCompleted()
            } else {
                startLogin()
            }
        }
    }

    override fun initInjector() {
        DaggerLoginComponent.builder()
            .applicationComponent(WFPApplication.applicationComponent)
            .presentationLoginModule(PresentationLoginModule(this))
            .build()
            .inject(this)
    }

    private fun startLogin() {
        startActivityForResult(googleSignIn.signInIntent, RC_SIGN_IN);
    }

    override fun loginCompleted() {
        startActivity(Intent(this, MenuPlanActivity::class.java))
        finish()
    }

    override fun showLoginError(msg: String) {
        Logger.d { "$TAG an error has occurred while logging: $msg" }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Logger.d { "[$TAG] result $requestCode" }

        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)!!
                presenter.login(account)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Logger.w{"$TAG Google sign in failed: $e"}
                showLoginError(e.toString())
            }
        }
    }
}