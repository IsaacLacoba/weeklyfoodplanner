package com.isaaclacoba.weeklyfoodplanner.domain.menu

import com.isaaclacoba.weeklyfoodplanner.domain.repository.RxBaseRepository
import io.reactivex.Observable


class MenuResource (
    private val searchMenuRepository: RxBaseRepository<String, SearchMenuItem>
) {
    private var lastRequestedMenus : MutableMap<String, Observable<MenuItem>> = mutableMapOf()

    fun searchForMenu(date: String): Observable<MenuItem> {
        if(lastRequestedMenus.containsKey(date))
            return lastRequestedMenus[date]!!

        return searchMenuRepository.getByKey(date)
                .map { it.items }
                .doOnNext { lastRequestedMenus[date] = Observable.just(it) }
    }
}