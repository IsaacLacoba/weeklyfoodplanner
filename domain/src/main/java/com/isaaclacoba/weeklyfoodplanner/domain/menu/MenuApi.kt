package com.isaaclacoba.weeklyfoodplanner.domain.menu

import io.reactivex.Observable

interface MenuApi {
    fun searchForMenu(date: String): Observable<SearchMenuItem>
}