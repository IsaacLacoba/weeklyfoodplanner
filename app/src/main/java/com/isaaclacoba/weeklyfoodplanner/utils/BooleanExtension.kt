package com.isaaclacoba.weeklyfoodplanner.utils

infix fun Boolean.then(action: () -> Unit): Boolean {
    if (this) {
        action()
    }
    return this
}